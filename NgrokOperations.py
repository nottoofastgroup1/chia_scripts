import os
import time
import DiscordOperations
import GetArgument


class NgrokOperations:
    def __init__(self):
        self.path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'ngrok.log')
        self.GetArgument = GetArgument.GetArgument()

    def delete_ngrok_log_file_if_exist(self):
        if os.path.isfile(self.path):
            os.remove(self.path)

    def start_ngrok(self):
        self.delete_ngrok_log_file_if_exist()
        ngrok_start_command = 'ngrok tcp 22 --region eu --log=stdout >> {} &'.format(self.path)
        os.system(ngrok_start_command)
        time.sleep(5)

    def get_ngrok_address(self):
        with open(self.path) as f:
            file = f.readlines()
        for line in file:
            for word in line.split():
                if 'url=tcp://' in word:
                    return word[10:]

    def is_ngrok_activated(self):
        active_process = os.popen('ps -aux | grep ngrok')
        return True if 'tcp 22 --region eu --log=stdout' in active_process.read() else False

    def run_ngrok_and_send_message(self):
        self.start_ngrok()
        DiscordOperations.discord_send_message(self.GetArgument.get_channel_id('prywatny'),
                                               'New address of {} is: {}'.format(self.GetArgument.get_device_name(),
                                                                                 self.get_ngrok_address()))
