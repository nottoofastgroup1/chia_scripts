import os


class InternetConnection:
    def check_ping(self):
        while True:
            ping_result = os.popen("ping -c 1 google.com")
            for line in ping_result.readlines():
                if ' 0% packet loss' in line:
                    return True
