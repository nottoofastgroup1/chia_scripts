import os


class GetArgument:
    def __init__(self):
        self.user = os.popen('whoami').read().replace('\n', '')
        self.device_name = {'myanme': 'ploter', 'chia-pm': 'farmer', 'maciek': 'testowy'}
        self.channel_id = {'ogolny': 861029524333920296, 'spam': 862249212603269121, 'prywatny': 860816790665953294,
                           'adresy': 876896616131469322}

    def get_device_name(self):
        return self.device_name.get(self.user)

    def get_channel_id(self, chanel_name):
        return self.channel_id.get(chanel_name)
