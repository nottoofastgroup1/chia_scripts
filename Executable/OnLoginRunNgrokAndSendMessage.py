import sys
import pathlib
directory = str(pathlib.Path(__file__).absolute().parent.parent) + r'/'
sys.path.append(directory)
import DiscordOperations
import NgrokOperations
import InternetConnection
import GetArgument


GetArgument = GetArgument.GetArgument()
NgrokOperations = NgrokOperations.NgrokOperations()
InternetConnection = InternetConnection.InternetConnection()

if not NgrokOperations.is_ngrok_activated():
    NgrokOperations.delete_ngrok_log_file_if_exist()
    InternetConnection.check_ping()
    NgrokOperations.start_ngrok()
    message = 'New address of {} is: {}'.format(GetArgument.get_device_name(), NgrokOperations.get_ngrok_address())
    DiscordOperations.discord_send_message(GetArgument.get_channel_id('ogolny'), message)
else:
    message = 'Someone is logging into {}'.format(GetArgument.get_device_name())
    DiscordOperations.discord_send_message(GetArgument.get_channel_id('ogolny'), message)
